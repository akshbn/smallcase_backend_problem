const goose = require('mongoose')

var tradeSchema = new goose.Schema({
    order: {
        stock:{
          type: goose.Schema.Types.ObjectId,
          ref:'Stock'
        },
        quantity: {
          type: Number,
          required: true
        },
        action: {
          type: String,
          required: true
        },
        dateOfTrade: {
          type: Date,
          default: Date.now
        },
        price: {
          type: Number,
          required: true
        }
      },
    portfolio: {
      type: goose.Schema.Types.ObjectId,
      ref: 'Portfolio'
    }

  },{  collection: 'trade'});

  module.exports = goose.model('Trade',tradeSchema);
