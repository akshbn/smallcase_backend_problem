const goose = require('mongoose')

var stocklistSchema = new goose.Schema({
    name: {
      type: String,
      required: true
    },
    ticker: {
      type: String,
      required: true,
      unique: true,
      validate: {
        validator: function(v) {
          return /\w+/.test(v);
        }
      }
    },
    sector: {
      type: String,
      required: true,
      validate: {
        validator: function(v) {
          return /\w+/.test(v);
        }
      }
    },

  },{  collection: 'stocklist'});

module.exports = goose.model('Stock',stocklistSchema);
