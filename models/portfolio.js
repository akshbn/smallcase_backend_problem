const goose = require('mongoose')
const Schema = goose.Schema;

var portfolioSchema = new goose.Schema({
    owner: {
      type: String,
      required: true
    },
    holding: [{
      stock:{
        type: Schema.Types.ObjectId,
        ref:'Stock'
      },
      quantity: {
        type: Number,
        default: 0
      },
      price: {
        type: Number,
        default: 0
      },
      totalBuy: {
        type: Number,
        default: 0
      }
    }]
},{collection: "portfolio"});

module.exports = goose.model('Portfolio',portfolioSchema);
