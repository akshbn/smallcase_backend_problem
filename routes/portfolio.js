const express = require('express')
var router = express.Router()
const Portfolio = require('../models/portfolio')
const Trade = require('../models/trade')
const Stock = require('../models/stocklist')
const bodyParser = require('body-parser')

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

/**
* updatePortfolioAfterSell is a helper function used to
*   deduct the quantity of a particular stock for the given portfolio
*   after the given trade is performed.
* folio - Portfolio object
* quantity - number indicating the quanitiy of stock to be updated
*  in the portfolio
* trade - Trade object
* response - server response object
*/
function updatePortfolioAfterSell(folio,quantity,stockId,trade,response){
  let folioHolding = folio.holding;
  let holdingId =0,quant = 0;
  for(let holdingIter=0;holdingIter<folioHolding.length;holdingIter++) {
    if(String(folioHolding[holdingIter].stock) === String(stockId)) {
      holdingId = folioHolding[holdingIter]._id;
      quant = Number(folioHolding[holdingIter].quantity)-Number(quantity);
    }
  }
  if(holdingId === 0){
    return {success:false,message:"stock not in portfolio"};
  }
  else {
    if(quant>0) {
      Portfolio.update({'holding._id':holdingId,_id:folio._id},{$set:{'holding.$.quantity':quant}},(err,val)=>{
        console.log("portfolio updated");
        let cumulativeReturn = (Number(trade.order.price) - 100)*Number(trade.order.quantity);
        response.json({success:true,data:{tradeId:trade._id,portfolio:folio._id,cumulativeReturn:cumulativeReturn}});
      });
    }
    else {
      Portfolio.update({_id:folio._id},{$pull:{holding:{_id:holdingId}}},{safe:true},(err,val)=>{
        console.log("stock removed from portfolio");
        let cumulativeReturn = (Number(trade.order.price) - 100)*Number(trade.order.quantity);
        response.json({success:true,data:{tradeId:trade._id,portfolio:folio._id,cumulativeReturn:cumulativeReturn}});
      });
    }
  }
}

/**
* updatePortfolioAfterBuy is a helper function used to
*   add to the quantity of a particular stock for the given portfolio
*   after the given trade is performed. Average buy price is also
*   calculated and updated.
* folio - Portfolio object
* quantity - number indicating the quanitiy of stock to be updated
*  in the portfolio
* trade - Trade object
* response - server response object
*/
function updatePortfolioAfterBuy(folio,quantity,stockId,trade,response){
  let folioHolding = folio.holding;
  let holdingId = 0,quant = 0, totalBuy = 0, price = 0;
  for(let holdingIter=0;holdingIter<folioHolding.length;holdingIter++) {
    if(String(folioHolding[holdingIter].stock) === String(stockId)) {
      holdingId = folioHolding[holdingIter]._id;
      quant = Number(folioHolding[holdingIter].quantity)+Number(quantity);
      totalBuy = Number(folioHolding[holdingIter].totalBuy) + Number(quantity);
      price = ((Number(folioHolding[holdingIter].price)*Number(folioHolding[holdingIter].totalBuy))+(Number(trade.order.price)*Number(quantity)))/totalBuy;
      price = parseFloat(price.toFixed(2));
      console.log("float "+price);
    }
  }
  // If stock does not exist in the portfolio, add it to the holdings
  if(holdingId === 0){
    Portfolio.update({_id:folio._id},{$push:{holding:[{stock:stockId,quantity:quantity,price:trade.order.price,totalBuy:quantity}]}},{upsert:true},(err,val)=>{
      if (err) {
        response.json({success:false,message:"stock could not be added to the portfolio"});
      }
      else {
        console.log("added");
        let cumulativeReturn = (100 - Number(trade.order.price))*Number(trade.order.quantity);
        response.json({success:true,data:{tradeId:trade._id,portfolio:folio._id,cumulativeReturn:cumulativeReturn}});
      }
    });
  }
  else {
    console.log("trade price "+trade.order.price);
    console.log("price "+price);
    console.log("totalBuy "+totalBuy);
    Portfolio.update({'holding._id':holdingId,_id:folio._id},{$set:{'holding.$.quantity':quant,'holding.$.totalBuy':totalBuy,'holding.$.price':price}},(err,val)=>{
      if(err) {
        console.log(err);
        response.json({success:false});
      }
      else {
      console.log("portfolio updated");
      let cumulativeReturn = (100 - Number(trade.order.price))*Number(trade.order.quantity);
      response.json({success:true,data:{tradeId:trade._id,portfolio:folio._id,cumulativeReturn:cumulativeReturn}});
      }
    });
  }
}

// This route returns the holdings in the portfolio
router.post('/holdings',(req,res)=>{
  let portfolioId = req.body.portfolioId;
  console.log(portfolioId);
  try {
    Portfolio
      .findOne({_id:portfolioId})
      .populate('holding.stock','ticker') // ensures the stock field is more readable to the user
      .exec((err,result) =>{
          if (err) {
            res.json({success:false,message:"Portfolio not found"});
          }
          else {
            res.json({success:true,data:result});
                }
          });
    } catch (errors) {
        res.status(500).send("There seems to be an issue. Why dont you try again in some time?");
    }
});

// This route is used to get the portfolio details as well as all trades for that portfolio
router.get('/',(req,res)=>{
  let portfolioId = req.query.portfolioId;
  try {
        Trade
      .find({portfolio:portfolioId})
      .populate('order.stock','ticker')
      .exec((err,trades) => {
        if (err) {
          res.json({success:false, message:"check the details sent and try again"});
        }
        else {
          Portfolio
          .findById(portfolioId)
          .populate('holding.stock','ticker')
          .exec((err,folio) => {
            if(err) {
              res.json({success:false, message:"check the details sent and try again"});
            }
            else {
              res.json({success:true,portfolioDetails:folio,trades:trades});
            }
          });
        }
    });
  } catch (err) {
    res.status(500).send("There seems to be an issue. Why dont you try again in some time?");
  }
});

// Creates a new portfolio with the given stock in holdings
router.post('/create',(req,res) => {
  let owner = req.body.owner;
  let ticker = req.body.ticker;
  let quantity = req.body.quantity;
  let price = req.body.price;
  console.log(ticker);
  try {
    for(let i=0;i<ticker.length;i++){
    console.log(ticker[i]);
    Stock.findOne({ticker:ticker[i]},(err,stock)=>{
      if (err) res.send(err);
      else {
        Portfolio.create({owner:owner,holding:[{stock:stock._id,quantity:quantity[i],price:price,totalBuy:quantity[i]}]},(err,val)=>{
          if (err) {
            res.json({success:false,message:"Could not create portfolio."});
          }
          else {
            console.log("added");
            res.json({success:true,data:val});
            }
          });
        }
      });
    }
  } catch (err) {
    res.status(500).send("There seems to be an issue. Why dont you try again in some time?");
  }
});

// Perform buy trade
router.post('/addtrade',(req,res)=>{
  let portfolioId = req.body.portfolioId;
  let ticker = req.body.ticker;
  let quantity = req.body.quantity;
  let price = req.body.price;
  try {
    Portfolio.findOne({_id:portfolioId},(err,folio) => {
      for(let reqIter=0;reqIter<ticker.length;reqIter++) {
        console.log(ticker[reqIter]);
        Stock.findOne({ticker:ticker[reqIter]},(err,stock)=>{
          if (err) res.json({success:false,message:"stock does not exist"});
          else{
            Trade.create({portfolio:folio._id,order:{stock:stock._id,quantity:quantity[reqIter],action:"buy",price:price}},(err,val) =>{
              if(err) {
                res.json({success:false,message:err});
              }
              else {
                updatePortfolioAfterBuy(folio,quantity[reqIter],stock._id,val,res);
              }
            });
          }
        });
      }
    });
  } catch (err) {
    res.status(500).send("There seems to be an issue. Why dont you try again in some time?");
  }
});

// Update a previously triggered trade. Trade action (buy/sell) change is not permitted.
router.post('/updatetrade',(req,res)=>{
  let portfolioId = req.body.portfolioId;
  let ticker = req.body.ticker;
  let quantity = req.body.quantity;
  let tradeId = req.body.tradeId;
  let price = req.body.price;
  try {
    Portfolio.findOne({_id:portfolioId},(err,folio) => {
    for(let reqIter=0;reqIter<ticker.length;reqIter++) {
      console.log(ticker[reqIter]);
      Stock.findOne({ticker:ticker[reqIter]},(err,stock)=>{
        if (err) {
          res.json({success:false,message:"stock does not exist"});
        }
        else{
          Trade.findOne({_id:tradeId},(err,specificTrade) =>{
            stockQuantity = specificTrade.order.quantity;
            let quant = Number(quantity[reqIter]) - Number(stockQuantity);
            if (specificTrade.order.action === "sell") {
              let folioHolding = folio.holding;
              let holdingId = 0;
              for(let holdingIter=0;holdingIter<folioHolding.length;holdingIter++) {
                if(String(folioHolding[holdingIter].stock) === String(stock._id)) {
                  if ((Number(folioHolding[holdingIter].quantity) - quant) < 0) {
                    res.json({success:false,message:"sell quantity is more than the quantity of stock in the portfolio "});
                    return;
                  }
                }
              }
            }
            console.log(stockQuantity);
            Trade.findOneAndUpdate({_id:tradeId},{$set:{order:{stock:stock._id,price:price,quantity:quantity[reqIter],action:specificTrade.order.action,dateOfTrade:Date.now()}}},(err,val)=>{
              if(err) {
                 res.json({success:false,message:"update trade failed"});
               }
              else {
                if(specificTrade.order.action === "buy") {
                    updatePortfolioAfterBuy(folio,quant,stock._id,specificTrade,res);
                }
                else {
                    updatePortfolioAfterSell(folio,quant,stock._id,specificTrade,res);
                }
              }
            });
            });
          }
        });
      }
    });
  } catch (err) {
    res.status(500).send("There seems to be an issue. Why dont you try again in some time?");
  }
});

// Perform sell trade
router.post('/selltrade',(req,res)=>{
  let portfolioId = req.body.portfolioId;
  let ticker = req.body.ticker;
  let quantity = req.body.quantity;
  let price = req.body.price;
  try {
    for(let reqIter=0;reqIter<ticker.length;reqIter++) {
      console.log(ticker[reqIter]);
      Stock.findOne({ticker:ticker[reqIter]},(err,stock)=>{
        if (err) {
          res.json({success:false,message:"stock does not exist"});
        }
        else{
          Portfolio.findById({_id:portfolioId},(err,folio) => {
            let folioHolding = folio.holding;
            let holdingId = 0;
            for(let holdingIter=0;holdingIter<folioHolding.length;holdingIter++) {
              if(String(folioHolding[holdingIter].stock) === String(stock._id)) {
                if ((Number(folioHolding[holdingIter].quantity) - Number(quantity[reqIter])) >= 0) {
                  Trade.create({portfolio:folio._id,order:{stock:stock._id,quantity:quantity[reqIter],action:"sell",price:price}},(err,val) =>{
                    if(err) {
                      res.json({success:false,message:"trade failed"});
                    }
                    else {
                      updatePortfolioAfterSell(folio,quantity[reqIter],stock._id,val,res);
                    }
                  });
                }
                else {
                  res.json({success:false,message:"sell quantity is more than the quantity of stock in the portfolio "});
                }
              }
            }
          });
        }
      });
    }
  } catch (err) {
  res.status(500).send("There seems to be an issue. Why dont you try again in some time?");
  }
});

module.exports = router;
