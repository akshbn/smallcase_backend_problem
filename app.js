const goose = require('mongoose')
const express = require('express')
const portfolioRouter = require('./routes/portfolio')


const app = express();
app.use('/portfolio',portfolioRouter);


goose.connect("mongodb://localhost/smallcase");
goose.Promise = global.Promise;

var db = goose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/', (req,res) => {
  res.send("Welcome to smallcase backend api");
});

app.listen(3000, () => {console.log('Smallcase server listening on port 3000!')});
