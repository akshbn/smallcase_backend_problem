# Smallcase Backend Problem

A set of enpoints satisfying requirements provided

### Endpoints

**/portfolio**

Method: GET

Inputs:
* portfolioId - Portfolio ID

Outputs:
* `{success:true, portfolioDetails: p, trades: t}`
  * p - portfolio object
  * t - trade object. All trades for the given portfolio
* `{success:false,message:"An error message here"}`

**/portfolio/create**

Method: POST

Inputs:
* owner - Name of the user the portfolio belongs to
* ticker - Stock ticker
* quantity - Number of shares of stock
* price - price at which the stock was bought

Outputs:
`{success:true, data: portfolioObject}` or `{success:false,message:"An error message here"}`

**/portfolio/holdings**

Method: POST

Inputs:
* portfolioId - Portfolio ID

Outputs:
`{success:true, data: portfolioObject}` or `{success:false,message:"An error message here"}`

**/portfolio/addtrade**

Method: POST

Inputs:
* portfolioId - Portfolio ID
* ticker - Stock ticker/symbol
* quantity - Number of shares to be bought
* price - Price at which shares are bought

Outputs:
* `{success:true,data:{tradeId:t,portfolio:p,cumulativeReturn:c}}`
  * t - ID of trade
  * p - portfolio ID
  * c - cumulative return of trade
* `{success:false,message:"An error message here"}`

**/portfolio/selltrade**

Method: POST

Inputs:
* portfolioId - Portfolio ID
* ticker - Stock ticker/symbol
* quantity - Number of shares to be sold
* price - Price at which shares are sold

Outputs:
* `{success:true,data:{tradeId:t,portfolio:p,cumulativeReturn:c}}`
  * t - ID of trade
  * p - portfolio ID
  * c - cumulative return of trade
* `{success:false,message:"An error message here"}`

**/portfolio/updatetrade**

Method: POST

Inputs:
* portfolioId - Portfolio ID
* ticker - Stock ticker/symbol
* quantity - Number of shares to be bought
* price - Price at which shares are bought
* tradeId - ID of trade to update

Outputs:
* `{success:true,data:{tradeId:t,portfolio:p,cumulativeReturn:c}}`
  * t - ID of trade
  * p - portfolio ID
  * c - cumulative return of trade
* `{success:false,message:"An error message here"}`

## Existing Data in DB

Direct data dumps from the DB

### stocklist

> Note: You can use any stock to populate the db till it satisfies the schema

```
{
	"_id" : ObjectId("5ad30a8460071c203b5607b1"),
	"name" : "HDFC Bank",
	"ticker" : "HDFCBANK",
	"sector" : "Banking"
}
{
	"_id" : ObjectId("5ad30da960071c203b5607b2"),
	"name" : "Kotak Mahindra Bank",
	"ticker" : "KOTAKBANK",
	"sector" : "Banking"
}
{
	"_id" : ObjectId("5ad30dd460071c203b5607b3"),
	"name" : "Bharat Petroleum",
	"ticker" : "BPCL",
	"sector" : "OMC"
}
{
	"_id" : ObjectId("5ad30de560071c203b5607b4"),
	"name" : "Hindustan Petroleum",
	"ticker" : "HPCL",
	"sector" : "OMC"
}
{
	"_id" : ObjectId("5ad30df960071c203b5607b5"),
	"name" : "ITC",
	"ticker" : "ITC",
	"sector" : "FMCG"
}
```

### portfolio

Data dump from db

> Note: You can use /portfolio/create to create a new portfolio

```
{
	"_id" : ObjectId("5adcb1aad939b916e1315d75"),
	"owner" : "Ak",
	"holding" : [
		{
			"quantity" : 9,
			"price" : 52.55,
			"totalBuy" : 11,
			"_id" : ObjectId("5adcb1aad939b916e1315d76"),
			"stock" : ObjectId("5ad30df960071c203b5607b5")
		}
	],
	"__v" : 0
}
```
